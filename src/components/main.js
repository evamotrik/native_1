import React from 'react';
import {StyleSheet, Button, View } from 'react-native';

export const Main = ({ navigation}) => {

  return (
    <View style={styles.view}>
      <Button title="Show users info" onPress={() => navigation.navigate('Registration', {isRegistered : true})} ></Button>
      <Button title="Logout" onPress={() => navigation.navigate('Login')}  color="#f194ff"></Button>
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});






