import React, { useState } from 'react';
import {Text, TouchableHighlight, StyleSheet, TextInput, Button, View } from 'react-native';

export const Login = ({ navigation }) => {

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  return (
    <View style={styles.view} >
      <TextInput placeholder="Login"
        value={login}
        onChangeText={text => setLogin(text)}
        style={styles.input} />
      <TextInput placeholder="Password"
        value={password}
        onChangeText={text => setPassword(text)}
        style={styles.input} />

      <Button title="Log in" onPress={() => navigation.navigate('Main')} />
      <TouchableHighlight onPress={() => navigation.navigate('Registration')} >
          <Text>Registration</Text>
      </TouchableHighlight>
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  input: {
    height: 40,
    marginBottom: 5,
    width: 200,
    borderColor: 'gray',
    borderWidth: 1,
    padding: 5
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
});


