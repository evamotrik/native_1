import React, { useContext, useEffect, useState } from 'react';
import {Alert, Switch, StyleSheet, Text, TextInput, Button, View } from 'react-native';
import Checkbox from 'expo-checkbox';
import Context from '../utils/context'

export const Registration = ({navigation, route}) => {

const context = useContext(Context);
const {isRegistered} = route.params;

useEffect(() => {
  if(isRegistered){
    setLogin(context.userState.login),
    setPassword(context.userState.password)
}
}, [])

    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [isCheckedMale, setCheckedMale] = useState(true);
    const [isCheckedFemale, setCheckedFemale] = useState(false);
    const toggleCheckBox = () => {
        setCheckedMale(previousState => !previousState);
        setCheckedFemale(previousState => !previousState);
    }
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);

    const showAlert = () => {
        if (login === '' || password === ''){
            return  Alert.alert(`Fields shouldn't be empty!`
            )
        }
        navigation.navigate('Main')
       
    }

    return (
        <View style={styles.view}>
                <TextInput placeholder="Login"
                    value={login}
                    onChangeText={text => setLogin(text)}
                    style={styles.input} />
                <TextInput placeholder="Password"
                    value={password}
                    onChangeText={text => setPassword(text)}
                    style={styles.input} />
                <View style={styles.container}><Text>Male</Text>
                    <Checkbox
                        style={styles.checkbox}
                        value={isCheckedMale}
                        onValueChange={toggleCheckBox}

                    />
                    <Text>Female</Text>
                    <Checkbox
                        style={styles.checkbox}
                        value={isCheckedFemale}
                        onValueChange={toggleCheckBox} />
                </View>

                <View style={styles.containerSwitch}>
                    <Text>Are you married?</Text>
                    <Switch
                        trackColor={{ false: '#767577', true: '#81b0ff' }}
                        thumbColor={isEnabled ? '#f5dd4b' : '#f4f3f4'}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={toggleSwitch}
                        value={isEnabled}
                    />
                </View>
                <Button title={(route.params.isRegistered === true ? 'Sing up' : 'Save')} 
                onPress={showAlert} />
        </View>


    )
}

const styles = StyleSheet.create({
     view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    input: {
        height: 40, 
        marginBottom: 5, 
        width: 200, 
        borderColor: 'gray', 
        borderWidth: 1, 
        padding: 5
    },

    container: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: 'row'
      },
      checkboxContainer: {
        flexDirection: "row",
        marginBottom: 20,
      },
      checkbox: {
        alignSelf: "center",
      },
      label: {
        margin: 8,
      },
      containerSwitch: {
        alignItems: 'center',
        justifyContent: 'center',
      },
});


