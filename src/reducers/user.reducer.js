export const defaultState = {}

export const ReducerFunction = (state = defaultState, action) => {
    switch (action.type) {
        case 'SHOW_INFO':
            let newState = {
                login: action.data.login,
                password: action.data.password
            };
            return newState;
        default:
            return state;
    }

};
