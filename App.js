import React, {useReducer} from 'react';
import { Registration } from './src/components/registration';
import { Login } from './src/components/login';
import { Main } from './src/components/main';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Context from "./src/utils/context";
import * as ACTIONS from "./src/actionCreators/user.action";
import { ReducerFunction, defaultState } from "./src/reducers/user.reducer";

export default function App() {
  const Stack = createNativeStackNavigator();

  const [stateUser, dispatchUserReducer] = useReducer(ReducerFunction, defaultState);

  const handleShowInfo = (data) => {
      dispatchUserReducer(ACTIONS.showInfo(data));
  };

  return (
    <Context.Provider
      value={{
        userState: stateUser,
        handleShowInfo: (data) => handleShowInfo(data),
      }}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen name="Registration" component={Registration} initialParams={{ isRegistered: false }} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Main" component={Main} />
        </Stack.Navigator>
      </NavigationContainer>
    </Context.Provider>
  );
}

